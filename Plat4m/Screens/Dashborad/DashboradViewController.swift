//
//  DashboradViewController.swift
//  Plat4m
//
//  Created by Lukáš Fečke on 03/10/2018.
//  Copyright © 2018 Lukáš Fečke. All rights reserved.
//

import UIKit
import Hero

class DashboradViewController: UIViewController {
    
    private var viewModel = DashboradViewModel()
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var timeTableCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupHero()
    }
    
    private func setupHero() {
        hero.isEnabled = true
        
        playButton.hero.id = Constants.HeroModifiers.playButtonToViewAndButton
        playButton.hero.modifiers = [.rotate()]
        
        menuButton.imageView?.hero.id = Constants.HeroModifiers.menuButtonImageToMenuButton
        menuButton.imageView?.contentMode = .scaleAspectFit
    }
}
