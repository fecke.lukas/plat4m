//
//  CountDownViewController.swift
//  Plat4m
//
//  Created by Lukáš Fečke on 03/10/2018.
//  Copyright © 2018 Lukáš Fečke. All rights reserved.
//

import UIKit
import Hero

class CountDownViewController: UIViewController {
    
    private var viewModel = CountDownViewModel()
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var taskButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var timeTableCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupHero()
    }
    
    private func setupHero() {
        hero.isEnabled = true

        stopButton.hero.modifiers = [.translate(y:500)]
        stopButton.hero.id = Constants.HeroModifiers.playButtonToViewAndButton
        bgView.hero.id = Constants.HeroModifiers.playButtonToViewAndButton
        
        menuButton.hero.id = Constants.HeroModifiers.menuButtonImageToMenuButton
        menuButton.imageView?.contentMode = .scaleAspectFit
    }
}
