//
//  ProjectsViewController.swift
//  Plat4m
//
//  Created by Lukáš Fečke on 03/10/2018.
//  Copyright © 2018 Lukáš Fečke. All rights reserved.
//

import UIKit
import Hero

class ProjectsViewController: UIViewController {
    
    private var viewModel = ProjectsViewModel()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var projectsCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupHero()
    }
    
    private func setupHero() {
        hero.isEnabled = true
        
        view.hero.id = Constants.HeroModifiers.playButtonToViewAndButton
        
        cancelButton.hero.modifiers = [.translate(y:500)]
//        cancelButton.hero.id = Constants.HeroModifiers.stopToCancel
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
