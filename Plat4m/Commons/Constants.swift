//
//  Constants.swift
//  Plat4m
//
//  Created by Lukáš Fečke on 03/10/2018.
//  Copyright © 2018 Lukáš Fečke. All rights reserved.
//

import Foundation

struct Constants {
    
    struct HeroModifiers {
        
        static let stopToCancel = "stopToCancel"
        static let playButtonToViewAndButton = "playButtonToViewAndButton"
        static let menuButtonImageToMenuButton = "menuButtonImageToMenuButton"
        
    }
    
}
